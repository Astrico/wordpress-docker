# Wordpress with Docker

Run: `docker-compose up -d`  
The wordpress service will be running at port 9980


To scale, run `docker-compose scale wordpress=n` with n as your preferred number of nodes  
Example: `docker-compose scale wordpress=3`
